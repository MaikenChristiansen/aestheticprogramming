let dots = [];
//define my object dots

function setup(){
createCanvas(windowWidth,windowHeight);
background(20);
frameRate(500);
dots.push(new dot(0,0));
}

function draw(){
for (var i = 0; i < dots.length; i++) {
if (dots[i].x > windowWidth) {
 dots[i].xdirection = dots[i].xdirection *(-1);
 dots[i].color = color(165,85,16);
}
if (dots[i].y > windowHeight) {
 dots[i].ydirection = dots[i].ydirection *(-1);
 dots[i].color = color(196,210,163);

}
if (dots[i].x < 0) {
 dots[i].xdirection = dots[i].xdirection *(-1);
 dots[i].color = color(165,85,16);

}
if (dots[i].y < 0) {
 dots[i].ydirection = dots[i].ydirection *(-1);
 dots[i].color = color(196,210,163);

}
  dots[i].move();
  dots[i].display();
}


}

function dot(x,y){
  this.x = x;
  this.y = y;
  this.xdirection = 3;
  this.ydirection = 3;
  this.color = color(165,85,16);



  this.display = function() {
    noFill();
    stroke(this.color);
    strokeWeight(0.1);
    ellipse(this.x,this.y,600);

  }

  this.move = function() {
    this.x = this.x + this.xdirection;
    this.y = this.y + this.ydirection;

}
}
