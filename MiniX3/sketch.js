//Naming the variable and array
let circles = [];


function setup() {
 createCanvas(windowWidth, windowHeight);
  //create a canvas/foundation
 frameRate(200);
  //setting the framework, so the circles moves smoothly


 for (let i = 0; i < 1000; i++)
 //create a loop starting at 0, execute it
 //check to see if it's less than a thousand, go up by 1, checking again
    circles[i] = {
    //Adding i in our array of circles

      x: random(0, width),
      y: random(0, height),
      //Placing the circles randomly on the entire canvas


//Ellipses
      display: function() {
        strokeWeight(1);
        stroke(78,109,162);
        noFill();
        ellipse(this.x, this.y, 30, 30);
        
      },

  //Moving the ellipses
      move: function() {
        this.x = this.x + random(-1, 1);
        this.y = this.y + random(-1, 1);
      }
      }
      }

      function draw() {
        background(6,6,6);
        for (let i = 0; i < circles.length; i++){
          //Adding to the array, refering to previous loop
        circles[i].move();
        circles[i].display();

//Text
        noStroke();
        textSize(150);
        textAlign(CENTER);
        fill(200);
        text('WAIT...',700,700);
      }
      }

      function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
