
class Circle {
  constructor(x, y, r) {
    this.x = x;
    this.y = y;
    this.r = 20;
    this.brightness = 0;
  }

  clicked(px, py) {
    let d = dist(px, py, this.x, this.y);
    if (d < this.r) {
      return true;
    } else {
    return false;
  }
}

  move() {
    this.x = this.x + random(-2, 2);
    this.y = this.y + random(-2, 2);
  }

  show() {
    strokeWeight(1);
    stroke(88,119,172);
    fill(68,99,152)
    ellipse(this.x, this.y, this.r);
  }
}
